﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IpcCommon
{
    public static class Constants
    {
        public const string DEFAULT_IP_ADDRESS = "127.0.0.1";
        public const int DEFAULT_PORT = 11111;

        public const int TIMEOUT = 2 * 1000;
    }
}
