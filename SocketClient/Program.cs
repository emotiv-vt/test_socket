﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketClient
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = IpcCommon.Constants.DEFAULT_PORT;
            if (args.Length > 1)
            {
                int result = 0;
                bool success = int.TryParse(args[0], out result);
                if (success
                    && result > 0)
                {
                    port = result;
                }
            }

            try
            {
                Console.WriteLine("Start Client");
                var client = new SocketClient(IpcCommon.Constants.DEFAULT_IP_ADDRESS, port);
                client.GetData();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.WriteLine("End Client");
            }
        }
    }

    internal sealed class SocketClient
    {
        private readonly string ipAddress;
        private readonly int port;

        public SocketClient(string ipAddress, int port)
        {
            this.ipAddress = ipAddress;
            this.port = port;
        }

        public void GetData()
        {
            TcpClient tcpClient = new TcpClient(ipAddress, port);
            try
            {
                tcpClient.ReceiveTimeout = IpcCommon.Constants.TIMEOUT;
                tcpClient.SendTimeout = IpcCommon.Constants.TIMEOUT;
                if (tcpClient.Connected)
                {
                    Stream stream = tcpClient.GetStream();
                    tcpClient.NoDelay = true;
                    var reader = new BinaryReader(stream);
                    var writer = new BinaryWriter(stream);

                    int sendData = 1;
                    Console.WriteLine($"Send data '{nameof(sendData)}': {sendData}");
                    writer.Write(sendData);

                    int recievedData = reader.ReadInt32();
                    Console.WriteLine($"Received data '{nameof(recievedData)}': {recievedData}");
                    return;
                }

                Console.WriteLine("Client not connected");
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                Console.WriteLine("Close client");
                tcpClient.Close();
            }

            Console.ReadLine();
        }
    }
}
