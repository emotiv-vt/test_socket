﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SocketServer
{
    class Program
    {
        static void Main(string[] args)
        {
            int port = IpcCommon.Constants.DEFAULT_PORT;
            if (args.Length > 1)
            {
                int result = 0;
                bool success = int.TryParse(args[0], out result);
                if (success
                    && result > 0)
                {
                    port = result;
                }
            }

            try
            {
                Console.WriteLine("Start Server");
                var server = new SocketServer(IpcCommon.Constants.DEFAULT_IP_ADDRESS, port);
                server.StartListening();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.WriteLine("End Server");
            }

            Console.ReadLine();
        }
    }

    internal sealed class SocketServer
    {
        private Stack<int> dataToSend = new Stack<int>();

        private string ipAddress;
        private int port;

        private TcpListener tcpListener;

        private Exception e;

        private bool isContinue;

        public SocketServer(string ipAddress, int port)
        {
            this.ipAddress = ipAddress;
            this.port = port;

            OpenSocketPort();

            Console.WriteLine("DataToSend: { ");
            for (var i = 1; i <= 10; i++)
            {
                Console.Write($"{i} ");
                dataToSend.Push(i);
            }

            Console.WriteLine("}");
            Console.WriteLine();

            isContinue = true;
        }

        private void OpenSocketPort()
        {
            try
            {
                IPAddress ipAddressObj = IPAddress.Parse(ipAddress);
                IPEndPoint localEndPoint = new IPEndPoint(ipAddressObj, this.port);

                tcpListener = new TcpListener(localEndPoint);

                tcpListener.Start();
            }
            catch (System.Exception e)
            {
                throw;
            }
        }

        public void StartListening()
        {
            try
            {
                Console.WriteLine("Start Listening");

                while (this.ShouldContinue())
                {
                    if (e != null)
                    {
                        Console.WriteLine("An error occurred during execution");
                        throw e;
                    }

                    if (dataToSend.Count == 0)
                    {
                        isContinue = false;
                        break;
                    }

                    Console.WriteLine("Waiting for an incoming connection.");

                    Socket socket = tcpListener.AcceptSocket();
                    socket.NoDelay = true;
                    socket.SendTimeout = IpcCommon.Constants.TIMEOUT;
                    //socket.ReceiveTimeout = IpcCommon.Constants.TIMEOUT;

                    Task.Run(() => Execute(socket));
                }
            }
            catch (System.Exception e)
            {
                throw;
            }
            finally
            {
                Console.WriteLine("Close Server");
                tcpListener.Stop();
                Console.WriteLine("End Listening");
            }
        }

        private bool ShouldContinue()
        {
            return e is null
                && isContinue;
        }
        private void Execute(Socket socket)
        {
            if (dataToSend.Count < 2)
            {
                isContinue = false;
                return;
            }

            try
            {
                Console.WriteLine("Socket got connected");

                var stream = new NetworkStream(socket);
                var reader = new BinaryReader(stream);
                var writer = new BinaryWriter(stream);

                int receivedData = reader.ReadInt32();
                Console.WriteLine($"Received data '{nameof(receivedData)}': {receivedData}");

                int sendData = dataToSend.Pop();
                Console.WriteLine($"Send data '{nameof(sendData)}': {sendData}");
                writer.Write(sendData);

                sendData = dataToSend.Pop();
                Console.WriteLine($"Send data '{nameof(sendData)}': {sendData}");
                writer.Write(sendData);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception when communicating with client");
                this.isContinue = false;
                this.e = e;
            }
            finally
            {
                Console.WriteLine("Close socket");
                socket.Close();
            }
        }
    }
}
